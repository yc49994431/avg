{
    "code": 0,
    "msg": "",
    "data": [
        [{
            "title": "首页",
            "icon": "layui-icon-home",
            "jump": "/"
        }, {
            "title": "车辆档案",
            "icon": "layui-icon-file",
            "jump": "/comment/list"
        }, {
            "title": "参数设置",
            "icon": "layui-icon-chart",
            "jump": "aboutshop/list"
        }, {
            "title": "定位标记",
            "icon": "layui-icon-top",
            "jump": "accusation/list"
        }, {
            "title": "特殊区域",
            "icon": "layui-icon-find-fill",
            "jump": "appointment/list"
        }, {
            "title": "状态监测",
            "icon": "layui-icon-chart-screen",
            "jump": "dm/list"
        }, {
            "title": "远程控制",
            "icon": "layui-icon-engine",
            "jump": "shop/list"
        }, {
            "title": "故障处理",
            "icon": "layui-icon-util",
            "jump": "product/list"
        }, {
            "title": "设备管理",
            "icon": "layui-icon-release",
            "jump": "testproduct/list"
        }, {
            "title": "用户管理",
            "icon": "layui-icon-user",
            "jump": "user/user/list"
        }, {
            "title": "客户管理",
            "icon": "layui-icon-username",
            "jump": "user/administrators/list"
        }],



        [{
            "title": "首页",
            "icon": "layui-icon-home",
            "jump": "/"
        }, {
            "title": "车辆档案",
            "icon": "layui-icon-file",
            "jump": "/comment/list"
        }, {
            "title": "参数设置",
            "icon": "layui-icon-chart",
            "jump": "aboutshop/list"
        }, {
            "title": "定位标记",
            "icon": "layui-icon-top",
            "jump": "accusation/list"
        }, {
            "title": "特殊区域",
            "icon": "layui-icon-find-fill",
            "jump": "appointment/list"
        }, {
            "title": "状态监测",
            "icon": "layui-icon-chart-screen",
            "jump": "dm/list"
        }, {
            "title": "远程控制",
            "icon": "layui-icon-engine",
            "jump": "shop/list"
        }, {
            "title": "故障处理",
            "icon": "layui-icon-util",
            "jump": "product/list"
        }, {
            "title": "设备管理",
            "icon": "layui-icon-release",
            "jump": "testproduct/list"
        }, {
            "title": "用户管理",
            "icon": "layui-icon-user",
            "jump": "user/user/list"
        }]
    ]
}